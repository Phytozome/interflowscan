#!/usr/bin/env nextflow

import interflowscan.parser.*
import interflowscan.filter.*
import interflowscan.processor.*

import org.apache.log4j.*
import groovy.util.logging.*
import groovy.transform.Synchronized

Logger log = Logger.getInstance(getClass())

params.in = 'interflow.fa'
params.xml = params.in +'.xml'
params.interproscanData = '/global/dna/projectdirs/plant/tools/interproscan/data/'
params.interproscanBin = '/global/dna/projectdirs/plant/tools/interproscan/bin/'
params.blastBinDir = '/blast/2.2.31'
params.interproXML = params.interproscanData+'interpro/72.0/interpro.xml'
params.properties = '/global/u1/j/jcarlson/workspace/InterFlowScan/InterFlowScan.properties'
params.analyses = 'kegg,kog,panther,pfam,phobius,pirsf,prints,prodom,prosite,coils,gene3d,signalp_euk,smart,superfamily,tigrfam,tmhmm'


def finalXML = params.xml
if (!finalXML.startsWith('/')) {
  finalXML = "$workflow.launchDir/" + finalXML
}

def copyParams = [:]
params.each { a,b -> copyParams[a] = b }

// process all properties and do parameter substitution
Properties rawProp = new Properties()
Properties prop = new Properties()
File propFiles = new File(params.properties)
propFiles.withInputStream {
  rawProp.load(it)
}

// we need to copy parameters. for some reason
rawProp.propertyNames().each {
            engine = new groovy.text.SimpleTemplateEngine()
            prop.setProperty(it,engine.createTemplate(rawProp.getProperty(it)).make(copyParams).toString())
            }


// open and split input
input = files(params.in)

analyses = params.analyses.tokenize(',')
runStep = Channel.create()
input.each { fasta ->
             analyses.each { progName ->
                             fasta.splitFasta( by: new Integer(prop."chunkSize.$progName"),
                                             into: runStep,
                                        autoClose: false) {seq -> tuple(seq,progName) }}}

runStep.close()

// the analysis step
process runAnalysis {

    executor = 'slurm'
    cpus = 8
    clusterOptions '--qos=genepool -A plntanal -t 12:00:00 --mem 64000'

    tag { "$progName" }

    input:
      set file(fa),val(progName) from runStep

    output:
      set val(progName), file("${progName}.out") into runOutput

    script:
      binPieces = prop."progBin.$progName".split(/,/)
      optPieces = prop."progOpt.$progName".split(/,/)
      command = "${params.interproscanBin}/${binPieces[0]} ${optPieces[0]} $fa"
      for(i=1;i<binPieces.size(); i++ ) {
        command += " | ${params.interproscanBin}/${binPieces[i]} ${optPieces[i]} "
      }

      """
      #!/bin/bash -l
      module load python
      source activate perlenv
      $command > ${progName}.out 2> stderr.out
      """

}

process parseAnalysis {

    tag { "$progName" }

    input:
      set val(progName),file(progOut) from runOutput

    output:
      set val(progName),file("${progName}.xml") into runFilters

    script:
      command = "groovy -cp ${workflow.projectDir}/lib/ ${workflow.projectDir}/lib/interflowscan/parser/ParserFactory.groovy ${progName} ${progOut} ${progName}.xml"
      """
      #!/bin/bash -l
      module load groovy
      $command
      """
}

process filterAnalysis {

    tag { "$progName" }

    input:
      set val(progName),file(xmlOut) from runFilters

    output:
      set val(progName),file("${progName}_filtered.xml") into addDomains

    script:
      command = "groovy -cp ${workflow.projectDir}/lib/:${workflow.projectDir}/lib/sqlite-jdbc-3.8.11.2.jar ${workflow.projectDir}/lib/interflowscan/filter/FilterFactory.groovy ${progName} ${xmlOut} ${progName}_filtered.xml "+prop."filterOpt.${progName}"
      """
      #!/bin/bash -l
      module load groovy
      $command
      """
}

process addXref {

    tag { "$progName" }

    input:
      set val(progName),file(filterOut) from addDomains

    output:
      set file("${progName}_xref.xml"),val(progName) into mergeXML

    script:
      command = "groovy -cp ${workflow.projectDir}/lib/ ${workflow.projectDir}/lib/interflowscan/processor/InterProXrefFactory.groovy "+prop."progOpt.interpro" +" ${progName} ${filterOut} ${progName}_xref.xml"

      """
      #!/bin/bash -l
      module load groovy
      $command
      """
}

mergeOut = Channel.create()
def xmlToMerge = []
mergeXML.subscribe( onNext: {f,d -> xmlToMerge << [d,f]},
                    onComplete: {
                             mergeOut << XMLMerger.process(xmlToMerge);
                             mergeOut.close()
                            }
                )

process finalFilter {

    executor = 'slurm'
    cpus = 1
    clusterOptions '--qos=genepool -A plntanal -t 12:00:00 --mem 64000'

    tag { "finalFilter" }

    input:
      file mergeOut from mergeOut

    script:
      command = "groovy -cp ${workflow.projectDir}/lib/sqlite-jdbc-3.8.11.2.jar "+
                "${workflow.projectDir}" + prop."progOpt.postMerge" + " " + prop."filterOpt.postMerge" + " -output ${finalXML} ${mergeOut}"
      """
      #!/bin/bash -l
      module load groovy
      $command
      """

}

#InterFlowScan

---

InterFlowScan implements the protein analysis components of
[InterProScan5](http://www.ebi.ac.uk/Tools/pfa/iprscan5/help/) using
[Nextflow](http://www.nextflow.io/)

*Is this needed?*

No. This is an unofficial implementation of the InterProScan 5 suite of analyses

*Why was this done?*

We have seen problems when attempting to run the analyses of InterProScan5
on very large sets of proteins. The JMS message service failed after
10 hours when running on our cluster. We could not diagnose the issue,
and instead looked at Nextflow as an alternate workflow management system.

*Is it a replacement for InterProScan5?*

No. This is not intended as a direct replacement. The web service aspects
of the codebase are not implemented, nor is the lookup service. All
analyses are done on all input sequences without any checking for
previous calculations.

The output is a XML file of the merged results from all protein together
with the Interpro domain cross references. It is a (largely) a subset
of the XML created by InterProScan5.

##Usage

First, download InterProScan5 to obtain the individual programs and data files
used in the analyses. Install the optional components as desired.

After downloading this component, review the files `InterFlowScan.nf` and
`InterFlowScan.properties` for system dependent settings. The important
things to look at are:

+ `chunkSize.<program name>` in `InterFlowScan.properties`

These parameters determine the number of sequences in each program run.

+ `progBin.<program name>` and `progOpt.<program name>` in `InterFlowScan.properties`

These are the paths to executable programs for each analysis. These
normally need to be updated only with a new release. The different
executables and data files will normally be installed with a common root
directory. The paths in the properties files is relative to this root.

For the analyses which have a single executable program the progBin entry
is a single string. For analyses which require multiple successive steps,
the different steps are separated by commas. The elements of progOpt are
designed so that the executable will read the input as the last token
in the first line of the command string and write to stdout

+ `params.interproscanBin`, `params.interproscanData` in `InterFlowScan.nf`

These are the common root locations for the executable and data
directories as mentioned previously. These can be overridden at run time
with command line options.

+ `params.analyses` in `InterFlowScan.nf`

This is the comma-separated list of analyses to run by default. If you
have not installed the optional programs, remove them from this list. The
list of programs can be determined with a run time option.

##Installation

Every attempt has been made to keep the binary programs identical to
the standard InterProScan5 distribution. Some minor tweaks of the Perl
scripts is always needed in order to install InterProScan 5 (to set the location of the Perl interpreter,
for example). The changes needed for this InterFlowScan are:

+ change the final line of superfamily.pl to ` print cat $cwd/$file.html `
+ install the perl script `hmmToSSF` (in the utils directory) to <bindir>/gene3d.
+ (until a bug in Bio::SearchIO::hmmer3 is fixed) install `parserHack` to <bindir>/gene3d
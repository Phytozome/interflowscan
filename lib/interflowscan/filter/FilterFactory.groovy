package interflowscan.filter

class FilterFactory {

  static main(String[] args) {
    // if we're called as a program,
    // the first argument is the filter type,
    // the second is the input
    // and the third is the ouput
    // and the fourth is the options
    if (args.size() > 2) {
      def parser = new FilterFactory().getFilter(args[0]);
      def xmlOut = new File(args[2]);
      if (args.size()>3) {
        xmlOut.write(parser.process(new File(args[1]),args[3]));
      } else {
        xmlOut.write(parser.process(new File(args[1]),''));
      }
    }
  }

  def loader

  FilterFactory() {
    loader = this.getClass().getClassLoader()
  }

  Class getFilter(type) {

    // change this_program_name to thisProgramName
    def bits = type.split('_');
    def modType = bits[0]
    if ( bits.size() > 1) {
      bits[1..-1].each{s->modType += s[0].toUpperCase()+s[1..-1]}
    }

    try {
      def instance = loader.loadClass('interflowscan.filter.'+modType+'Filter')
    } catch (ClassNotFoundException e) {
      def instance = loader.loadClass('interflowscan.filter.NoOpFilter')
    }
  }
}

package interflowscan.filter

/* 
 * a no-op filter. This does nothing but return the passed object
 */

class NoOpFilter {

  static process(b,opts) {
    b.text
  }
}

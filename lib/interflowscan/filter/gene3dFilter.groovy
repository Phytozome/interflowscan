package interflowscan.filter

import groovy.xml.XmlUtil

/*
 * We need to scan the csv file (specified in the option) and
 * replace the model names with the Gene3D identifier
 */
class gene3dFilter {

  static process(b,opts) {
    def modelToName = [:]
    def csv = new File(opts)
    csv.eachLine { line ->
      def fs = line.tokenize(',')
      // remove enclosing quotes (if there are any)
      (1..fs.size()).each { fs[it-1] = fs[it-1].replaceFirst(/^['"]/,'').replaceFirst(/['"]$/,'') }
      modelToName[fs[0]] = ['G3DSA:'+fs[1],fs[2..-1].join(',')]
    }

    assert modelToName.size() > 0

    def gene3dXML = new XmlParser().parse(b.newDataInputStream())

    // scan through all hits and change model to names
    gene3dXML.protein.matches.match.signature.each { node ->
      //println "processing node: "+node.@ac
      // old style was just an id: i.e. "2a0uB01".
      // style in 4.0.1 as db identifiers: cath|4_1_0|16pkA01/5-192"
      // but then we go back to just id.
      def tokens = node.@ac.tokenize('\\|/')
      // unnamed models will not be included.
      if (tokens.size() > 0 && modelToName[tokens.size()==4?tokens[2]:tokens[0]] != null) {
        def (ac,name) = modelToName[tokens.size()==4?tokens[2]:tokens[0]]
        if (name != null && name != 'null') node.@name = name
        if (ac != null && ac != 'null') node.@ac = ac
      }
    }
    XmlUtil.serialize(gene3dXML)
  }
}

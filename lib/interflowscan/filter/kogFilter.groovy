package interflowscan.filter

import groovy.xml.XmlUtil

/*
 * We need to scan the tsv file (specified in the option) and
 * replace the GenBank accession with the KOG identifier
 */
class kogFilter {

  static process(b,opts) {
    def accToName = [:]
    def csv = new File(opts)
    csv.eachLine { line ->
      def fs = line.tokenize('\t')
      accToName[fs[0]] = [fs[1],fs[2]]
    }

    assert accToName.size() > 0

    def kogXML = new XmlParser().parse(b.newDataInputStream())

    // scan through all hits and change model to names
    kogXML.protein.matches.match.signature.each { node ->
      def (name,desc) = accToName[node.@ac]
      if (name != null && name != 'null') node.@ac = name
      if (desc != null && desc != 'null') node.@desc = desc
    }
    XmlUtil.serialize(kogXML)
  }
}

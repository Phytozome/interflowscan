package interflowscan.filter

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import groovy.xml.XmlUtil

/**
 * @author jcarlson
 * 
 * We need to scan the pfam hits and filter out overlapping
 * hits from the same clan
 * 
 * 1) if 2 pfam domains are nested, keep both
 * 2) if 2 pfam domains are in the same clan, keep only the higher scoring one
 */
class pfamFilter {

  static process(b,opts) {
    String clanFile
    String nestFile
    Connection nestDB
    Connection clanDB

    // primitive option parsing
    def words = opts?.tokenize(' +')
    def ctr = 0
    while ( ctr < words.size()) {
      if (words[ctr] == '-clan') {
        ctr++
        clanFile = words[ctr]
      }
      if (words[ctr] == '-nest') {
        ctr++
        nestFile = words[ctr]
      }
      ctr++
    }

    // load the sqlite driver
    try {
      Class.forName("org.sqlite.JDBC")
    } catch (Exception e) {
      println 'Cannot load sqlite driver. Add the jar file to NXF_CLASSPATH.'
      System.exit(1)
    }
    nestDB = openDB(nestFile,'nest')
    clanDB = openDB(clanFile,'clan')

    // the nested and clann db's are simple key-value store. The key is
    // whatever we picked up from the AC line. The value is the NE or MB
    // entry in Pfam-A.full

    def pfamXML = new XmlParser().parse(b.newDataInputStream())

    // the filtering procedure is this:
    // sort by starting coordinage
    // if one pfam domain is nested in another (according to the nest
    // membership and according to the coordinates), keep them both.
    // if multiple pfams are in the same clan, only keep the highest scoring
    // one.
    // scan through all proteins
    pfamXML.protein.each { protein ->
      if (protein.matches) {
        // make a copy of the hits along with a flag of whether to keep. (default 'y')
        def hits = []
        protein.matches[0].match.each { match ->
          match.locations[0].each { loc ->
            hits << [match.signature[0].@ac,loc,'Y']
          }
        }
        def ctr1 = 0;
        while ( ctr1 < hits.size()) {
          def ctr2 = ctr1+1;
          while (ctr2 < hits.size()) {
            if (  isOverlapping(hits[ctr1][1],hits[ctr2][1]) &&
            (!isNested(hits[ctr1],hits[ctr2],nestDB)) &&
            isInSameClan(hits[ctr1][0],hits[ctr2][0],clanDB) ) {
              if (hits[ctr1][1].@score <= hits[ctr2][1].@score) {
                hits[ctr1][2] = 'N'
              } else {
                hits[ctr2][2] = 'N'
              }
            }
            ctr2++
          }
          ctr1++
        }

        // now go through the xml and remove the nodes marked 'N'
        hits.each { hit ->
          if (hit[2] == 'N') {
            def rightHit = protein.matches[0].match.find { hitNode -> hitNode.signature[0].@ac == hit[0] }
            def rightLoc = rightHit?.locations[0].location.find { hitLoc -> hitLoc.@start == hit[1].@start && hitLoc.@end == hit[1].@end }
            rightHit?.locations[0]?.remove(rightLoc)
            if (rightHit?.locations[0]?.children().size()==0) {
              rightHit?.parent().remove(rightHit)
            }
          }
        }
      }
    }

    clanDB.close()
    nestDB.close()

    XmlUtil.serialize(pfamXML)
  }

  static boolean isOverlapping(a,b) {
    a.@end.toInteger() >= b.@start.toInteger() && a.@start.toInteger() <= b.@end.toInteger()
  }

  static boolean isNested(a,b,db) {
    // determine if a is nested in b (or vice versa)
    // We know they are overlapping. Find out which
    // is first
    boolean foundNested = false
    if (a[1].@start <= b[1].@start) {
      // b is nested in a
      def nested = getVals(a[0],db)
      nested.each { n ->
        if ( b[0] == n ) {
          foundNested = true
        }
      }
    } else if (b[1].@start <= a[1].@start) {
      // b is nested in a
      def nested = getVals(b[0],db)
      nested.each { n ->
        if ( a[0] == n ) {
          foundNested = true
        }
      }
    }
    return foundNested
  }
  static boolean isInSameClan(a,b,db) {
    def aClans = getKeys(a,db)
    def bClans = getKeys(b,db)
    boolean sameClan = false
    aClans.each { ac ->
      bClans.each { bc ->
        if (ac == bc) {
          sameClan = true;
        }
      }
    }
    return sameClan
  }

  static getKeys(value,db) {
    def strippedValue = value.tokenize('.')[0]
    try {
      Statement statement = db.createStatement()
      ResultSet rs = statement.executeQuery("select key from keyValue where value='$value' or value='$strippedValue'");
      def keys = []
      while(rs.next()) {
        keys << rs.getString("key")
      }
      return keys
    } catch(Exception e) {
      assert e.getMessage()
    }
  }

  static getVals(key,db) {
    def strippedKey = key.tokenize('.')[0]
    try {
      Statement statement = db.createStatement()
      ResultSet rs = statement.executeQuery("select value from keyValue where key='$key' or key='$strippedKey'");
      def vals = []
      while(rs.next()) {
        vals << rs.getString("value");
      }
      return vals
    } catch(Exception e) {
      assert e.getMessage()
    }
  }

  static openDB(String file,String dbName) {

    def dirname = (file[0]!='/'?:'/')+file.tokenize("/")[0..-2].join('/')
    def myDatabase

    try {
      // see if we can open a DB file in the same directory as
      // the specified file
      myDatabase = openNamedDB(dirname,dbName)
    } catch (FileNotFoundException) {
      myDatabase = createNamedDB(dirname,file,dbName)
    } catch (Exception e) {
      println 'Caught the unexpected exception '+e.getMessage()
      System.exit(1)
    }
    myDatabase
  }

  static createNamedDB(String dirname,String file,String dbName) {

    def fileName = dirname + '/' + dbName + '.sqlite'
    Connection connection = DriverManager.getConnection('jdbc:sqlite:'+fileName);
    connection.createStatement().executeUpdate("create table keyValue (key String, value String)")
    File f = new File(file)
    def currentKey
    def currentValue
    def fullMatch
    def ctr = 0
    f.eachLine { line ->
      if (line ==~ /#=GF AC\s+\S+.*/) {
        // new PF
        (fullMatch,currentKey) = (line =~ /^#=GF AC\s+(\S+)/)[0]
      } else if ( dbName == 'nest' && line ==~ /#=GF NE\s+.*/) {
        // new nest
        (fullMatch,currentValue) = (line =~ /^#=GF NE\s+(\w+);?/)[0]
        assert currentKey != null
        insert(connection,currentKey,currentValue)
      } else if ( dbName == 'clan' && line ==~ /#=GF MB\s+\S+.*/) {
        (fullMatch,currentValue) = (line =~ /^#=GF MB\s+(\w+);?/)[0]
        assert currentKey != null
        insert(connection,currentKey,currentValue)
      }
    }
    connection
  }

  static openNamedDB(String dirname,String dbName) throws Exception {

    def fileName = dirname + '/' + dbName + '.sqlite'
    def file = new File(fileName)
    if (!file.exists() ) throw new FileNotFoundException()

    Connection connection = DriverManager.getConnection('jdbc:sqlite:'+fileName)
  }

  static insert(Connection c, String key, String value) {
    c.createStatement().executeUpdate("insert into keyValue values (\"$key\",\"$value\")");
  }
}

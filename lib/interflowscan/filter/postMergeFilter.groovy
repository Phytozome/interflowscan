package interflowscan.filter

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import groovy.xml.XmlUtil

/**
 * @author jcarlson
 *
 * this is a catch-all to do all the filtering that needs to be
 * done after merging. We may want to do some mutual-best-hit picking,
 * so we need to have all the results from one analysis in one xml.
 * Or we may want to exclude things based on IPR domains. Whatever.
 *
 * Maybe this can be made modular. Until then, I'll just have all
 * the logic in here.
 */
class postMergeFilter {

  static main(String[] args) {
    // if we're called as a program.
    // the input xml is the final argument
    if (args.size() > 0) {
      String b = args[-1]
      args.size() > 1? process(new File(b),args[0..-2].join(' ')):process(new File(b))
    }
  }

  static process(b,opts) {

    /* KEGG filtering.
     * We compute the C-score:
     *   (score/max(max score for that subject,max score for that query))
     *   and demand this be above a threshold
     * and/or
     *   e-value below a threshold
     * and/or
     *   span on the query above a fractional threshold
     * and/or
     *   span on the subject above a fractional threshold
     */

    // the "default" is to accept everything
    Double evalueCutoff = Double.MAX_VALUE
    Double cscoreCutoff = 0.0
    Double queryCoverageCutoff = 0.0
    Double subjectCoverageCutoff = 0.0
    String output = 'results.xml'
    String mapFile = null

    // primitive option parsing
    def words = opts?.tokenize(' +')
    def ctr = 0
    while ( ctr < words.size()) {
      if (words[ctr] == '-evalue') {
        ctr++
        evalueCutoff = words[ctr].toDouble()
      } else if (words[ctr] == '-cscore') {
        ctr++
        cscoreCutoff = words[ctr].toDouble()
      } else if (words[ctr] == '-query_coverage') {
        ctr++
        queryCoverageCutoff = words[ctr].toDouble()
      } else if (words[ctr] == '-subject_coverage') {
        ctr++
        subjectCoverageCutoff = words[ctr].toDouble()
      } else if (words[ctr] == '-output') {
        ctr++
        output = words[ctr]
      } else if (words[ctr] == '-map') {
        ctr++
        mapFile = words[ctr]
      }
      ctr++
    }


    println 'parsing xml'
    def inputXML = new XmlParser().parse(b.newDataInputStream())
    println 'xml parsed.'

    def results = [];
    // pull out the kegg analysis results
    inputXML."kegg-results".each { kResults ->
      kResults."kegg-results-block".each {
        results << it.text()
      }
    }

    println results.size()+' results generated'

    // we will need this db
    try {
      Class.forName("org.sqlite.JDBC")
    } catch (Exception e) {
      println 'Cannot load sqlite driver. Add the jar file to (NXF_)CLASSPATH.'
      System.exit(1)
    }

    def mapDB = openDB(mapFile)
    def queryHigh = [:]
    def subjectHigh = [:]
    // pass 1: read and look for max's

    results.each { block ->
      block?.tokenize('\n').each { line ->
        // the fields are: query, hit, hsp_score, query_start, query_end,
        // subject_start, subject_end, hsp_evalue,
        // subject_length, query_length, num_matched
        // hsp_num_conserved + hsp_num_identical
        def fields = line.tokenize('\t')
        if (fields.size() > 2) {
          def query = fields[0]
          def subject = fields[1]
          def score = fields[2].toDouble()
          if (queryHigh[query]== null || queryHigh[query] < score ) {
            queryHigh[query] = score
          }
          if (subjectHigh[subject]== null || subjectHigh[subject] < score ) {
            subjectHigh[subject] = score
          }
        }
      }
    }
    println 'pass1 completed.'
    println queryHigh.size()+' queryHighs'
    println subjectHigh.size()+' subjectHighs'


    // pass 2: go though again and find which to keep.
    // toss this on a hash indexed by the query. The
    // value is an array of hits.
    // we keep things only if (1) the representative has one (or more
    // KEGG identifiers and (2) if the metrics indicate it is close
    // to a MBH
    def keepers = [:]
    int lineCtr = 0
    results.each { block ->
      block?.tokenize('\n').each { line ->
        lineCtr++
        def fields = line.tokenize('\t')
        if (fields.size() > 8) {
          def query = fields[0]
          def subject = fields[1]
          // this may return more that 1
          def mapVals = getKeys(subject,mapDB)
          mapVals.each { keggId ->
            def score = fields[2].toInteger()
            def cscore = fields[2].toDouble()/
                (queryHigh[query]>subjectHigh[subject]?queryHigh[query]:subjectHigh[subject])
            def subject_span = (fields[6].toDouble()-fields[5].toDouble()+1)/
                fields[8].toDouble()
            def query_span = (fields[4].toDouble()-fields[3].toDouble()+1)/
                fields[9].toDouble()
            def evalue = fields[7].toDouble()
            if (cscore >= cscoreCutoff && evalue <= evalueCutoff &&
            subject_span >= subjectCoverageCutoff && query_span >= queryCoverageCutoff) {
              // keeper node.
              if (keepers[query] == null) keepers[query] = [:]
              keepers[query][keggId] = ['score':score,'cscore':cscore,'start':fields[3],'end':fields[4],
                'subject_start':fields[5],'subject_end':fields[6],'evalue':evalue,
                'query_span':query_span,'subject_span':subject_span]
            }
          }
        }
        if (lineCtr%10000 == 0) println "Processed $lineCtr lines..."
      }
    }
    println 'pass2 completed.'

    // delete the analysis node
    inputXML."kegg-results".each { inputXML.remove(it) }
    println 'kegg-results removed'

    // construct new nodes
    keepers.each { query, hits ->
      def protNode = inputXML.protein.find { protein ->
        protein.xref.@id == query }
      if (protNode == null) {
        protNode = new Node(inputXML,'protein')
        new Node(protNode,'xref',['id':query])
        new Node(protNode,'matches')
      }
      hits.each { hit,loc ->
        def matchNode = new Node(protNode.matches[0],'kegg-match')
        new Node(matchNode,'signature',['ac':hit,'dbname':'KEGG'])
        new Node(new Node(matchNode,'locations'),'location',loc)
      }
    }

    println 'pass3 completed.'

    new XmlNodePrinter(new PrintWriter(new FileWriter(output))).print(inputXML)
  }

  static openDB(String fileName) {

    def myDatabase

    try {
      // see if we can open a DB file in the same directory as
      // the specified file
      myDatabase = openNamedDB(fileName)
    } catch (FileNotFoundException) {
      myDatabase = createNamedDB(fileName)
    } catch (Exception e) {
      println 'Caught the unexpected exception '+e.getMessage()
      System.exit(1)
    }
    myDatabase
  }

  static openNamedDB(String fileName) throws Exception {

    def dbName = fileName + '.sqlite'
    def file = new File(dbName)
    if (!file.exists() ) throw new FileNotFoundException()

    Connection connection = DriverManager.getConnection('jdbc:sqlite:'+dbName)
  }

  static createNamedDB(String fileName) {

    def dbName = fileName + '.sqlite'
    Connection connection = DriverManager.getConnection('jdbc:sqlite:'+dbName);
    connection.createStatement().executeUpdate("create table keyValue (key String, value String)")
    File f = new File(fileName)
    def currentKey
    def currentValue
    def fullMatch
    def ctr = 0
    f.eachLine { line ->
      def fields = line.tokenize('\t')
      def key = fields[0].replaceAll('ko:','')
      insert(connection,key,fields[1])
    }
    connection
  }


  static insert(Connection c, String key, String value) {
    c.createStatement().executeUpdate("insert into keyValue values (\"$key\",\"$value\")");
  }

  static getVals(key,db) {
    try {
      Statement statement = db.createStatement()
      ResultSet rs = statement.executeQuery("select value from keyValue where key='$key'");
      def vals = []
      while(rs.next()) {
        vals << rs.getString("value");
      }
      statement.close()
      return vals
    } catch(Exception e) {
      assert e.getMessage()
    }
  }


  static getKeys(value,db) {
    try {
      Statement statement = db.createStatement()
      ResultSet rs = statement.executeQuery("select key from keyValue where value='$value'");
      def keys = []
      while(rs.next()) {
        keys << rs.getString("key");
      }
      statement.close()
      return keys
    } catch(Exception e) {
      assert e.getMessage()
    }
  }

}

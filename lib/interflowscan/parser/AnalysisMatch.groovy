package interflowscan.parser
/*
 * AnalysisMatch: a class to contain all the details of a subject hit in an
 * analysis..
 * The properties kept are the name (which may be distinct from the accession identifier),
 * the description (a longer version), score, evalue, and a list of locations.
 *
 */
class AnalysisMatch {
  def matchName
  def matchDesc
  def matchLocs
  def matchScore
  def matchEvalue
  def dbName
  
  AnalysisMatch() {
    matchName = null
    matchDesc = null
    // these are used for 'global' score and e-value
    matchScore = null
    matchEvalue = null
    // the individual hit may have score and e-value, too.
    matchLocs = []
  }
  AnalysisMatch(String db) {
    dbName = db;
    new AnalysisMatch()
    matchDesc = null
  }
  // add to the location list a map that probably looks
  // like [start:XX,end:YY,score:ZZ...]
  // all of this gets copied into <location> attributes.
  void addLoc(a) {
    matchLocs << a
  }
}

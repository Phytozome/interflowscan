package interflowscan.parser

import interflowscan.parser.AnalysisMatch

/*
 * AnalysisMatches a class to contain all the detailed AnalysisMatch information.
 * We're going to maintain a map of individual AnalysisMatch object which are
 * indexed by a unique accession identifier for the hit. In addition, there is
 * a record of the current 'active' key in the map.
 * 
 */

class AnalysisMatches {
  
  def matches
  def currentMatch
  
  AnalysisMatches() {
    matches = [:]
    currentMatch = null
  }
  
  AnalysisMatches(String accession) {
    matches = [accession:new AnalysisMatch()]
    currentMatch = accession
  }
  
  AnalysisMatch addAnalysisMatch(String accession) {
    matches[accession] = new AnalysisMatch()
    currentMatch = accession;
    return matches[accession]
  }
  
  Boolean hasMatches() {
    return matches != null && matches.size() > 0
  }
  
  AnalysisMatch getAnalysisMatch(accession) {
    // returns the matches associated with an accession.
    // creates a new AnalysisMatch if it does not exist.
    if (!matches.containsKey(accession)) {
      matches[accession] = new AnalysisMatch()
    }
    currentMatch = accession
    return matches[accession]
  }
  
  AnalysisMatch getAnalysisMatch() {
    return matches[currentMatch]
  }
}

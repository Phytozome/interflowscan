package interflowscan.parser

import interflowscan.parser.*
class ParserFactory {

  static main(String[] args) {
    // if we're called as a program,
    // the first argument is the parser type,
    // the second is the input
    // and the third is the ouput
    if (args.size() > 2) {
      def parser = new ParserFactory().getParser(args[0]);
      def xmlOut = new File(args[2]);
      xmlOut.write(parser.parse(new File(args[1])));
    }
  }

  def loader

  ParserFactory() {
    loader = this.getClass().getClassLoader()

  }

  Class getParser(type) {

    // change this_program_name to thisProgramName
    def bits = type.split('_');
    def modType = bits[0]
    if ( bits.size() > 1) {
      bits[1..-1].each{s->modType += s[0].toUpperCase()+s[1..-1]}
    }

    def instance = loader.loadClass('interflowscan.parser.'+modType+'Parser')
  }
}

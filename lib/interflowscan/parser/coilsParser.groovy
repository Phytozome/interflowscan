package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class coilsParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'COILS') }
      def seqId = null
      def analysisMatches = null
      b.eachLine { str ->
        if (str ==~ />\s*\S+.*/) {
          // new protein. has format >Protein id
          def (full,newSeqId) = (str =~ />\s*(\S+)/)[0]
          if (seqId != newSeqId) {
            // This is probably not needed.
            // it should have been processed by // line (below)
            makeNode(xml,seqId,analysisMatches)
          }
          seqId = newSeqId
          analysisMatches = new AnalysisMatches()
        } else if (str ==~ /\d+\s+\d+/) {
          // expect
          // <number><space><number>
          def fields = str.split(/\s+/)
          analysisMatches.getAnalysisMatch('coils').dbName = 'COILS'
          analysisMatches.getAnalysisMatch('coils').addLoc(['start':fields[0],'end':fields[1]])
        } else if (str == '//') {
          // end of record for this sequence
          makeNode(xml,seqId,analysisMatches)
          seqId = null
          analysisMatches = null
        }
      }
      // final hit
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }

}

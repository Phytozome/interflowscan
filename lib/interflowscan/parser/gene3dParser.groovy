package interflowscan.parser

import groovy.xml.MarkupBuilder


import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch


class gene3dParser extends interflowscanParser {

  static def main(args) {
    def b= new File("data/gene3d.out"); 
    println parse(b);
  }
  
  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'CATHGENE3D') }
      def seqId = null
      def hitId = null
      def analysisMatches = new AnalysisMatches()
      b.eachLine { str ->
        def fields = str.split("\t");
        if (fields[0] != seqId ) {
          makeNode(xml,seqId,analysisMatches)
          analysisMatches = new AnalysisMatches()
        }
        seqId = fields[0]
        def limits = fields[14].tokenize(':')
        def ctr = 0
        while ( ctr < fields[13].toInteger() ) {
          analysisMatches.getAnalysisMatch(fields[1]).dbName = 'CATHGENE3D'
          analysisMatches.getAnalysisMatch(fields[1]).matchEvalue = fields[10]
          analysisMatches.getAnalysisMatch(fields[1]).addLoc(['start':limits[2*ctr],'end':limits[2*ctr+1]])
          ctr++
        }
      }
      if (analysisMatches?.hasMatches() ) {
        // final hit
        makeNode(xml,seqId,analysisMatches)
      }
    }

    writer.toString()+"\n"
  }

  }



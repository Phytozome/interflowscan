package interflowscan.parser

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

import groovy.xml.MarkupBuilder

class gff3Parser extends interflowscanParser {

  def static parse(b,dbname) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':dbname) }
      def seqId = null
      def analysisMatches = null
      b.eachLine { str ->
        if (str ==~ /#.*/) {
          // comment/header. Ignore
        } else {
          def fields = str.split('\t')
          if (seqId != fields[0]) {
            // out with the old.
            makeNode(xml,seqId,analysisMatches)
            seqId = fields[0]
            analysisMatches = new AnalysisMatches()
          }
          AnalysisMatch a = analysisMatches.getAnalysisMatch(fields[2])
          a.dbName = dbname
          a.addLoc(['start':fields[3],'end':fields[4]])
          fields[8].split(';').each { bit ->
            if (bit.trim() ==~ /\S+\s*[:=]?\s*\S+/) {
              def(full,key,value) = (bit.trim() =~ /(\S+)\s*[:=]?\s*(\S+)/)[0]
              def unquotedValue = value.trim().replaceFirst(/^"/,'').replaceFirst(/"$/,'')
              // different things that can be a name, score or evalue
              // (case insensitive matching here. Optional - in e-value
              if (key =~ /(?i)name/) {
                a.setMatchName(unquotedValue)
              } else if (key =~ /(?i)score/) {
                a.setMatchScore(unquotedValue)
              } else if (key =~ /(?i)e-?value/) {
                a.setMatchEvalue(unquotedValue)
              }
            }

          }
        }
      }
      // last one
      makeNode(xml,seqId,analysisMatches)
    }
    writer.toString()+"\n"
  }
}

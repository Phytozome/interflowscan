package interflowscan.parser

import groovy.xml.MarkupBuilder
import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class hmm2Parser extends interflowscanParser {


  def static parse(b,dbname) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':dbname) }
      def seqId = null
      def analysisMatches = null
      // there are 2 phases of parsing: one for hits, the
      // second for the locations
      def parsingHits = false
      def parsingMatches = false
      b.eachLine { str ->
        if ( str ==~ /Query sequence:\s+\S+.*/ ) {
          // new protein. has format ID<space>Protein ids
          def (fullLine,newSeqId) = (str =~ /^Query sequence:\s+(\S+)/)[0]
          if (seqId != newSeqId) {
            // out with the old. If needed
            makeNode(xml,seqId,analysisMatches)
          }
          seqId = newSeqId
          analysisMatches = new AnalysisMatches()
          parsingHits = false
          parsingMatches = false
        } else if (str ==~ /Scores for sequence.*/) {
          parsingMatches = true
        } else if (str ==~ /Parsed for domains.*/) {
          parsingHits = true
        } else if (str.trim() ==~ /Model\s+Description.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /Model\s+Domain.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /----.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /\[no hits.*/) {
          // no hits. Reset
          parsingMatches = false
          parsingHits = false
        } else if (str.trim().size() == 0) {
          // this signifies end of parsing matches.
          parsingMatches = false
          parsingHits = false
        } else if (str == "//") {
          // end of this sequence
          makeNode(xml,seqId,analysisMatches)
          parsingMatches = false
          parsingHits = false
          seqId = null
          analysisMatches = null
        } else {
          def f = str.trim().split(/\s+/)

          if (parsingMatches) {
            // first pass. split and add a hit
            analysisMatches.addAnalysisMatch(f[0])
            analysisMatches.getAnalysisMatch(f[0]).dbName = dbname
            analysisMatches.getAnalysisMatch(f[0]).setMatchEvalue(f[-2])
            analysisMatches.getAnalysisMatch(f[0]).setMatchScore(f[-3])
          } else if (parsingHits) {
            analysisMatches.getAnalysisMatch(f[0]).addLoc(['start':f[2],'end':f[3],'subject_start':f[5],'subject_end':f[6]])
          }
        }

      }
      // last one
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }

}



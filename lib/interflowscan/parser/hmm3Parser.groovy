package interflowscan.parser

import groovy.xml.MarkupBuilder
import java.nio.file.Files
import java.nio.charset.Charset
import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class hmm3Parser extends interflowscanParser {

  def static parse(b,dbname) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':dbname) }
      def seqId = null
      def hitId = null
      def analysisMatches = null
      def parsingMatches = false
      b.eachLine { str ->
        if ( str ==~ /Query:\s+\S+.*/ ) {
          // new protein. has format ID<space>Protein id
          def (fullLine,newSeqId) = (str =~ /^Query:\s+(\S+)/)[0]
          if (seqId != newSeqId) {
            // out with the old. If needed
            makeNode(xml,seqId,analysisMatches)
          }
          seqId = newSeqId
          analysisMatches = new AnalysisMatches()
          parsingMatches = false;
        } else if (str ==~ />>\s*\S+\s+.*/) {
          def fullLine
          def thisHitId
          def thisHitName
          assert analysisMatches != null
          (fullLine,thisHitId,thisHitName) = (str =~ /^>>\s*(\S+)\s+(.*)/)[0]
          analysisMatches.addAnalysisMatch(thisHitId)
          analysisMatches.getAnalysisMatch(thisHitId).matchName = thisHitName 
          analysisMatches.getAnalysisMatch(thisHitId).dbName = dbname
          parsingMatches = true;
        } else if (str.trim() ==~ /#.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /--.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /\[No individual.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /\[No hits.*/ ) {
          // nothing to do
        } else if (str.trim() ==~ /\[No targets.*/) {
          // no hits. Reset
          parsingMatches = false;
        } else if (str.trim().size() == 0) {
          // this signifies end of parsing matches.
          parsingMatches = false
        } else {
          def f = str.trim().split(/\s+/)
          if (parsingMatches) {
            analysisMatches.getAnalysisMatch().addLoc(['start':f[12],'end':f[13],'strand':1,
              'subject_start':f[6],'subject_end':f[7],'e-value':f[5],'score':f[2]])
          }
        }

      }
      // last one
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }

}



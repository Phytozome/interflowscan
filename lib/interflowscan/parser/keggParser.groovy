package interflowscan.parser
/*
 * The KEGG parser is different. We have a bunch of HSPs. We need to
 * accumulate all the HSPs and find the score of every query to every
 * subject. Then generate a score of hsp score / max( score_to_subject, score_to_query)
 * 
 */
import groovy.xml.MarkupBuilder
import groovy.xml.XmlUtil
import java.lang.Double
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class keggParser {

  def static parse(b) {

    // let's write some new xml
    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer)
    xml.setDoubleQuotes(true)

    // blast output is a tsv file.
    // scan through all hits and keep a record of query, hit, hspscore, hspevalue,
    // query_start, query_end, query_length, hit_length, hsp_num_conserved + hsp_num_identical
    // these are some fields that are needed in the post-merge filtering.
    // the blast+ option that give this (together with a header) is
    // -outfmt '7 qacc sacc score evalue qstart qend length positive qlen slen'

    // start with a gratuitous newline to make things look pretty
    StringBuffer data = new StringBuffer('\n')
    b.eachLine { str ->
      if (str ==~ /#.*/ || str.trim().size() == 0) {
        // header, comment or blank line. ignore.
      } else {
        // else it is data.
        data.append(str).append('\n')
      }
    }
    
    xml."protein-matches" { release { dbinfo('dbname':'KEGG') }
                             results(data.toString())  }
                           
    writer.toString()+"\n"
  }
}

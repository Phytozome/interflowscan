package interflowscan.parser
/*
 * The KOG parser is different. We have a bunch of HSPs. We need to
 * accumulate all the HSPs into a connected graph and see if they
 * span more than a certain threshold of coverage.
 * 
 */
import groovy.xml.MarkupBuilder
import groovy.xml.XmlUtil
import java.lang.Double
import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class kogParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    // blast output is xml
    def parser = new XmlParser(false,false,true)
    parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
    parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
    def kogXML = parser.parse(b.newDataInputStream())

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'KOG') }

      // scan through all hits and change model to names
      kogXML.BlastOutput_iterations[0].Iteration.each { node ->
        def seqId = node."Iteration_query-def".text().split("\\s")[0]
        def keepers = []
        node.Iteration_hits.Hit.each { hit ->
          def hitId = hit.Hit_accession[0].text()
          def hitDetails = []
          def subjectLength = hit.Hit_len[0].text().toInteger()
          hit.Hit_hsps[0].Hsp.each { hsp ->
            hitDetails << ['start':hsp."Hsp_query-from"[0].text(),
              'end':hsp."Hsp_query-to"[0].text(),
              'subject_start':hsp."Hsp_hit-from"[0].text(),
              'subject_end':hsp."Hsp_hit-to"[0].text(),
              'score':hsp.Hsp_score[0].text(),
              'e-value':hsp.Hsp_evalue[0].text() ]
          }
          // end of looping over HSP. see if this is a keeper
          // this returns non-null if it's above threshold
          def keeper = processHitDetails(hitDetails,subjectLength)
          if (keeper != null ) keepers << ['hitId':hitId, 'loc':keeper]
        }

        // end of looping over hits. process this node
        def bestkeeper = null
        keepers.each {
          bestkeeper = (bestkeeper == null || it.loc.score > bestkeeper.loc.score)?it:bestkeeper
        }
        if (bestkeeper != null) {
          def analysisMatches = new AnalysisMatches()
          // we'll keep everything with an e-value < 1000*best e-value
          Double bestEvalue = bestkeeper.loc['e-value'].toDouble()
          keepers.each {
            if ( it.loc['e-value'].toDouble() < 1000.0*bestEvalue) {
              analysisMatches.getAnalysisMatch(it.hitId).dbName = 'KOG'
              analysisMatches.getAnalysisMatch(it.hitId).addLoc(it.loc)
            }
          }
          makeNode(xml,seqId,analysisMatches)
        }
      }
    }
    writer.toString()+"\n"
  }

  static processHitDetails(list,length) {

    /* Construct a graph of every path that traverses the query. See
     * if the coverage is threshold and return the highest scoring hit
     * Ideally we should do this as a DP problem. But we're lazy.
     * Just enumerate every legitimate path. Let's hope
     * that the list is not too long
     */
    def allPaths = []
    // start by copying everything from the input list to the list of paths
    def pathCtr = 0
    list.each { allPaths << [pathCtr]
      pathCtr++
    }

    // now go the lists again and again until we cannot add anymore
    def addedOne = false
    def loopCtr = 1
    while (addedOne || loopCtr==1 ) {
      addedOne = false
      def addToPaths = []
      pathCtr = 0
      list.each { element ->
        allPaths.each { path ->
          // if an element starts before the first element of the path,
          // prepend this to the new paths
          if (path.size() == loopCtr &&
          element['end'].toInteger() < list[path[0]]['start'].toInteger() &&
          element['subject_end'].toInteger() < list[path[0]]['subject_start'].toInteger()) {
            addToPaths << [pathCtr,path].flatten()
            addedOne = true
          }
        }
        pathCtr++
      }
      loopCtr++
      addToPaths.each { allPaths << it }
    }
    // now see which (if any) exceed the length threshold and return the highest scoring one
    def bestPath = null
    def bestScore = 0
    allPaths.each { path ->
      def span = list[path[-1]]['subject_end'].toInteger() - list[path[0]]['subject_start'].toInteger() + 1
      if (span >= 0.7*length ) {
        def score = 0
        Double evalue = 10000
        path.each { score += list[it]['score'].toInteger()
          evalue = (evalue<list[it]['e-value'].toDouble())?evalue:list[it]['e-value'].toDouble()
        }
        if (score > bestScore ) {
          bestPath = ['start':list[path[0]]['start'],
            'end': list[path[-1]]['end'],
            'score': score,
            'e-value':evalue]
          bestScore = score
        }

      }

    }
    bestPath
  }

}
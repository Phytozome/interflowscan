package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class pantherParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'PANTHER') }
      def seqId = null
      def analysisMatches = null
      b.eachLine { str ->
        if (str.tokenize('\t').size() == 6) {
          def fields = str.tokenize('\t')
          if (fields[0] != seqId) {
            makeNode(xml,seqId,analysisMatches)
            seqId = fields[0]
            analysisMatches = new AnalysisMatches()
          }
          if (fields[5] ==~ /\d+-\d+/ ) {
            def (full,start,end) = (fields[5] =~ /(\d+)-(\d+)/)[0]
            analysisMatches.getAnalysisMatch(fields[1]).dbName = 'PANTHER'
            analysisMatches.getAnalysisMatch(fields[1]).addLoc(['start':start,'end':end,'score':fields[3],'e-value':fields[4]])
          }
        }
      }
      // final node
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }
}

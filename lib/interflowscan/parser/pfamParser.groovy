package interflowscan.parser

import groovy.xml.MarkupBuilder
import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch
import interflowscan.parser.hmm3Parser

class pfamParser extends hmm3Parser {

  def static parse(b) {
    parse(b,'PFAM')
  }
}



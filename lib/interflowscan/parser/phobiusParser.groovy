/**
 *
 */
package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

/**
 * @author jcarlson
 *
 */

class phobiusParser extends interflowscanParser {

  static def main(args) {
    def b= new File("data/phobius.out");
    println parse(b);
  }

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'PHOBIUS') }
      def seqId = null
      def analysisMatches = null
      b.eachLine { str ->
        if (str ==~ /ID\s+(\S+)/) {
          // new protein. has format ID<space>Protein id
          def (full,newSeqId) = (str =~ /ID\s+(\S+)/)[0]
          if (seqId != newSeqId) {
            // out with the old. Probably not needed here
            makeNode(xml,seqId,analysisMatches)
          }
          seqId = newSeqId
          analysisMatches = new AnalysisMatches()
        } else if (str ==~ /FT.*/) {
          // expect either:
          // FT<space>DOMAIN<space>lo<space>hi<space>type
          // FT<space>TRANSMEM<space>lo<space>hi
          // FT<space>SIGNAL<space>lo<space>hi
          def fields = str.split(/\s{2,}/)
          assert fields[1] == 'DOMAIN' || fields[1] == 'TRANSMEM'  || fields[1] == 'SIGNAL'
          if (fields[1] == 'DOMAIN') {
            fields[4].replace('.','')
            analysisMatches.getAnalysisMatch(fields[4].replace('.','')).addLoc(['start':fields[2],'end':fields[3]])
            analysisMatches.getAnalysisMatch().dbName = 'PHOBIUS'
          } else if (fields[1] == 'TRANSMEM') {
            analysisMatches.getAnalysisMatch('TRANSMEMBRANE').addLoc(['start':fields[2],'end':fields[3]])
            analysisMatches.getAnalysisMatch().dbName = 'PHOBIUS'
          } else if (fields[1] == 'SIGNAL') {
            analysisMatches.getAnalysisMatch('SIGNAL').addLoc(['start':fields[2],'end':fields[3]])
            analysisMatches.getAnalysisMatch().dbName = 'PHOBIUS'
          }
        } else if (str == '//') {
          // process
          makeNode(xml,seqId,analysisMatches)
          seqId = null
          analysisMatches = null
        }
      }
      // last one
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }

}



package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class pirsfParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'PIRSF') }
      def seqId = null
      def analysisMatches = null

      b.eachLine { str ->
        if (str ==~ /Query Sequence:\s+\S+\s+matches\s+\S+:\s*.*/) {
          def (full,newSeqId,newHitId,newHitName) = (str =~ /Query Sequence:\s+(\S+)\s+matches\s+(\S+):\s*(.*)/)[0]
          if ( seqId != newSeqId ) {
            // out with the old. If needed
            makeNode(xml,seqId,analysisMatches)
            seqId = newSeqId
            analysisMatches = new AnalysisMatches()
          }
          analysisMatches.getAnalysisMatch(newHitId)
          analysisMatches.getAnalysisMatch().matchName = newHitName
          analysisMatches.getAnalysisMatch().dbName = 'PIRSF'
        } else if (str ==~ /\s*and matches Sub-Family.*/) {
          def (full,newHitId,newHitName) = (str =~ /\s*and matches Sub-Family\s+(\S+):\s*(.*)/)[0]
          analysisMatches.getAnalysisMatch(newHitId)
          analysisMatches.getAnalysisMatch().matchName = newHitName
          analysisMatches.getAnalysisMatch().dbName = 'PIRSF'
        } else if (str ==~ /\s*#.*/) {
          // header lines for data
        } else if (str ==~ /\s*\d+\s+!\s+([0-9.\[\]e-]+\s+){12}.*/) {
          // number, a ! then a bunch of int's or floating point numbers (or '..' or '[.' ) separated by spaces
          def fields = str.trim().split(/\s+/)
          analysisMatches.getAnalysisMatch().addLoc(['start':fields[12],'end':fields[13],'subject_start':fields[6],'subject_end':fields[7]])
          analysisMatches.getAnalysisMatch().dbName = 'PIRSF'
        }
      }

      makeNode(xml,seqId,analysisMatches)

    }

    writer.toString()+"\n"
  }
}

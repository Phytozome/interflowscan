package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class printsParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    def trailingHashes = ~/#*$/
    def leadingHashes = ~/^#*/
    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'PRINTS') }
      def seqId = null
      def analysisMatches = null
      def motifNameToPR = [:]
      b.eachLine { str ->
        if (str ==~ /Sn;\s+\S+.*/) {
          def (full,newSeqId) = (str =~ /^Sn;\s+(\S+)/)[0]
          if (seqId == null) {
            analysisMatches = new AnalysisMatches()
          } else if (newSeqId != seqId) {
            makeNode(xml,seqId,analysisMatches);
            analysisMatches = new AnalysisMatches()
          }
          seqId = newSeqId
        } else if (str ==~ /1TBN NO SIGNIFICANT RESULTS.*/ ) {
          seqId = null
          analysisMatches = null
        } else if (str ==~ /1TBH\s+\S+\s+.*?\s+\S+.*/)  {
          def fields = str.trim().split("\\s+")
          // fields[0] is 1TBH
          def hitName = fields[1]
          def eValue = fields[2]
          def hitId = fields[-1]
          def desc = fields[3..-2].join(' ')
          motifNameToPR[hitName] = hitId
          analysisMatches.getAnalysisMatch(hitId).dbName = 'PRINTS'
          analysisMatches.getAnalysisMatch(hitId).matchName= hitName
          analysisMatches.getAnalysisMatch(hitId).matchEvalue = eValue
          analysisMatches.getAnalysisMatch(hitId).matchDesc = desc
        }  else if (str ==~ /3TBH.*/)  {
          //3TBT MotifName       No.Mots   IdScore PfScore Pvalue    Sequence       Len  low  pos   high
          //3TBH 3FE4SFRDOXIN    1  of  3  46.79   348     3.89e-05  VDEAVCIGCRYC    12   0    8     0
          def fields = str.trim().split("\\s+")
          def hitName = fields[1]
          def hitScore = fields[6]
          def eValue = fields[7]
          def hitStart = fields[11]
          def hitEnd = fields[11].toInteger() + fields[9].toInteger() - 1
          // are there extra unmatched characters at the beginning? these are #'s in the Sequence field.
          def extraUnmatchedAtStart = fields[8].length() - fields[8].replaceAll(leadingHashes,"").length();
          // or at the end?
          def extraUnmatchedAtEnd = fields[8].length() - fields[8].replaceAll(trailingHashes,"").length();
          // we REALLY don't expect to see a string of all hashes. But let's be sure this this would cause a fmin > fmax problem.
          def ctr = analysisMatches.getAnalysisMatch(motifNameToPR[hitName]).matchLocs.size();
          if ( fields[8].replaceAll(leadingHashes,"").length() > 0 ) {
            analysisMatches.getAnalysisMatch(motifNameToPR[hitName]).addLoc(['start':(hitStart.toInteger() + extraUnmatchedAtStart.toInteger()),
                                                                             'end':(hitEnd.toInteger() - extraUnmatchedAtEnd.toInteger())])
          }
        }
      }

      // final node
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }
}

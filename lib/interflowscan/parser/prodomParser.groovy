package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class prodomParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'PRODOM') }
      def seqId = null
      def analysisMatches = null
      b.eachLine { str ->
        if (str[0..1].equals("//") ) {
          // end of a sequence. process
          if (seqId != null) {
            makeNode(xml,seqId,analysisMatches)
            seqId = null
            analysisMatches = null
          }
        } else {
          // intermediate double slashes delimit fields
          def fields = str.split("//")
          def (full0,newSeqId,hitStart,hitEnd) = (fields[0] =~ /^(\S+)\s+(\d+)\s+(\d+)\s+/)[0]
          if (seqId == null ) {
            analysisMatches = new AnalysisMatches()
          } else if (newSeqId != seqId) {
            // ought to not happen
            makeNode(xml,seqId,analysisMatches)
            analysisMatches = new AnalysisMatches()
          }
          seqId = newSeqId
          def (full1,hitId) = (fields[1] =~ /^\s*(\S+)\s+.*/)[0]
          hitId = hitId.trim().split(";")[0].replaceAll("pd_","")
          analysisMatches.getAnalysisMatch(hitId).dbName = 'PRODOM'
          analysisMatches.getAnalysisMatch(hitId).addLoc(['start':hitStart,'end':hitEnd])
          fields[2].trim().split("\\s+").each {
            def (fTV,tag,value) = ( it =~ /^(.)=(\S+)/)[0]
            if (tag == "E") analysisMatches.getAnalysisMatch(hitId).setMatchEvalue(value)
            if (tag == "S") analysisMatches.getAnalysisMatch(hitId).setMatchScore(value)
          }
        }
      }

      // final node
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }
}

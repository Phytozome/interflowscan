/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.signalpParser

/**
 * @author joe
 *
 */
class signalpGramPositiveParser extends signalpParser {
  
  static parse(b) {
    parse(b,'SIGNALP')
  }

}

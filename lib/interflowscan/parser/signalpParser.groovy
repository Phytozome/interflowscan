package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class signalpParser extends interflowscanParser {

  def static parse(b,dbname) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':dbname) }
      def seqId = null
      def analysisMatches = null

      b.eachLine { str ->
        if ( str ==~ /Name=\S+\s+SP='YES' Cleavage site between pos. \d+ and \d+:.*/ ) {
          def (fullMatch,newSeqId,hitStart,hitEnd,hitId,hitScore) =
              (str =~ /Name=(\S+)\s+SP='YES' Cleavage site between pos. (\d+) and (\d+):\s+(\S+).*\s+D=(\S+)/ )[0]
          if (seqId != newSeqId ) {
            makeNode(xml,seqId,analysisMatches)
            seqId = newSeqId
            analysisMatches = new AnalysisMatches()
            analysisMatches.getAnalysisMatch(hitId).dbName = dbname
            analysisMatches.getAnalysisMatch(hitId).addLoc(['start':hitStart,'end':hitEnd])
          } else {
            analysisMatches.getAnalysisMatch(hitId).dbName = dbname
            analysisMatches.getAnalysisMatch(hitId).addLoc(['start':hitStart,'end':hitEnd])
          }
        }
      }
      makeNode(xml,seqId,analysisMatches)
    }
    writer.toString()+"\n"
  }
}

package interflowscan.parser

import groovy.xml.MarkupBuilder
import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch
import interflowscan.parser.hmm2Parser

class smartParser extends hmm2Parser {

  def static parse(b) {
    parse(b,'SMART')
  }
}

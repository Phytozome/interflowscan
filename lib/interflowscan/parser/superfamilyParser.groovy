package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class superfamilyParser extends interflowscanParser {
  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    // the file we get is html. We're going to parse it by hand;
    // look for <tr>...</tr> and split on these.

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'SSF') }
      def seqId = null
      def analysisMatches = null
      def lineToSplit = ''
      b.eachLine { str ->
        lineToSplit += str
        // do a non-greedy search for <tr>...</tr>
        def lines = (lineToSplit =~ /<tr>.*?<\/tr>/)
        if (lines.getCount() > 0) {

          // trim off the front all <tr> to </tr> (greedy this time)
          lineToSplit = lineToSplit.replaceFirst('.*<tr>.*<\\/tr>','')
          lines.each {
            // and each line has <th> </th> data. Never <td>
            def fields = ( it =~ /(<th>).*?(<\/th>)/)
            if (fields.getCount() > 0 ) {
              // bareFields are these without the <th> and </th>
              def bareFields = []
              fields.each {bareFields << it[0].replaceFirst('<th>','').replaceAll('</th>$','') }
              
              if ( (bareFields.size() == 8) && (bareFields[0] != "Seq_ID") ) {
                if (bareFields[0] != seqId ) {
                  if (seqId != null) {
                    makeNode(xml,seqId,analysisMatches);
                  }
                  seqId = bareFields[0]
                  analysisMatches = new AnalysisMatches()
                }
                def ranges = []
                if (bareFields[1].trim() != '-') {
                  bareFields[1].trim().split('<br>').each {
                   def (fullRange,hitStart,hitEnd) = (it.trim() =~ /(\d+)-(\d+)/)[0]
                    ranges << [hitStart,hitEnd]
                  }
                } else {
                  // no hit here.
                  seqId = null
                }
                if( seqId != null && ranges.size()) {
                  // the SSF id is hidden in the link in column 3. We need to add the 'SSF' prefix
                  def (fullLink,ssf) = (fields[3] =~ /.*sunid=(\d+)/)[0]
                  analysisMatches.getAnalysisMatch('SSF'+ssf).dbName = 'SSF'
                  ranges.each {
                    analysisMatches.getAnalysisMatch('SSF'+ssf).addLoc(['start':it[0],'end':it[1]])
                    //println seqId+'\t'+ssf+'\t'+it[0]+'\t'+it[1]
                  }
                  analysisMatches.getAnalysisMatch('SSF'+ssf).matchEvalue = bareFields[2]
                }
              }
            }
          }
        }
      }
      // final node
      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }
}

package interflowscan.parser

import groovy.xml.MarkupBuilder

import interflowscan.parser.interflowscanParser
import interflowscan.parser.AnalysisMatches
import interflowscan.parser.AnalysisMatch

class tmhmmParser extends interflowscanParser {

  def static parse(b) {

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer);

    xml.setDoubleQuotes(true)

    xml."protein-matches" {
      xml.release { dbinfo('dbname':'TMHMM') }
      def seqId = null
      def analysisMatches = null

      b.eachLine { str ->
        if (str ==~ /#.*/) {
          // meta-information. skip
        } else if (str ==~ /\S+\s+\S+\s+\S+\s+\d+\s+\d+.*/) {
          def (newSeqId,progName,hitClass,matchStart,matchEnd) = str.split(/\s+/)
          if (seqId != newSeqId) {
            // out with the old
            makeNode(xml,seqId,analysisMatches)
            seqId = newSeqId
            analysisMatches = new AnalysisMatches()
          }          
          analysisMatches.getAnalysisMatch(hitClass).dbName = 'TMHMM'  
          analysisMatches.getAnalysisMatch(hitClass).addLoc(['start':matchStart,'end':matchEnd])
        }
      }

      makeNode(xml,seqId,analysisMatches)
    }

    writer.toString()+"\n"
  }
}

package interflowscan.processor

import groovy.xml.XmlUtil
/*
 * PostMergeXref: add interpro xref's to xml for all program types.
 * This is intended as a post-processing correction step, mainly.
 */

class InterProPostMergeXref {

  def InterProPostMergeXref() {}

  def process(interproXML,filePath) {

    def analysisXML = new XmlParser().parse(filePath.newDataInputStream())

    def protCtr = 0
    // scan through all hits and add interpro domains
    analysisXML.protein.each { pNode ->
      protCtr++
      //if (protCtr % 1000 == 0) println 'scanned '+protCtr+' protein nodes...'
      pNode.matches.each { matches ->
        matches."prosite-match".each { mNode ->
          mNode.signature.each { sigNode ->
            //println 'looking for a hit to '+sigNode.@ac
            def ip = interproXML.interpro.member_list.db_xref.find { node ->
              node.@db == sigNode.@dbname &&
                  dbkeyAndAcMatches(node.@dbkey,sigNode.@ac)
            }
            if (ip != null ) {
              //println 'found.'
              if (sigNode.@name == null && ip.@name != null) {
                // add the name if we don't have one
                sigNode.@name = ip.@name
              }

              // ascend the xml until we find the parent from interpro
              // this should just be parent().parent()
              def parentNode = ip;
              def upOne = ip?.parent();
              while (parentNode != null && parentNode.name() != 'interpro' ) {
                parentNode = upOne;
                upOne = parentNode.parent();
              }
              if (parentNode != null) {
                // see if we have this already
                def haveAlready = false
                sigNode.entry.each { entry ->
                  if (entry.@id == parentNode.@id) haveAlready = true
                }
                if (!haveAlready) {
                  def newNode = new Node(sigNode,'entry')
                  newNode.@id = parentNode.@id
                  newNode.@name = parentNode.@short_name
                  // does this have GO terms?
                  def goNodes = parentNode.class_list.classification.findAll { node ->
                    node.@class_type == 'GO'}
                  goNodes?.each { goNode -> def newGoNode = new Node(newNode,'go-xref')
                    // some twists and turns here
                    newGoNode.@id = goNode.@id
                    newGoNode.@db = 'GO'
                    newGoNode.@category = goNode.category.text().toUpperCase().replaceAll(' ','_')
                    newGoNode.@name = goNode.description.text()
                  }
                } else {
                  //println 'already have it.'
                }
              } else {
                //println 'cannot determine parent.'
              }
            } else {
              //println 'not found.'
            }
          }
        }
      }
    }
    //println 'scanned '+protCtr+' protein nodes.'
    XmlUtil.serialize(analysisXML)
  }

  // this encapsulates how we compare dbkey in the interpro.xml file
  // to the 'ac' from the match hit name in the program run. This may
  // be program specific and will be overridden in a subclass
  def dbkeyAndAcMatches(String a,String b) {
    return a==b
  }
}

package interflowscan.processor

import groovy.xml.XmlUtil

class InterProXref {

  def InterProXref() {}

  def process(interproXML,filePath) {

    def analysisXML = new XmlParser().parse(filePath.newDataInputStream())

    // add the db version
    def dbName = analysisXML.release.dbinfo[0].attributes()['dbname']

    // is this output from this program in the xref file?
    // if not, there's no need to process it.
    def dbIsInXref = false
    analysisXML.release.dbinfo.each { db -> def entry = interproXML.release.dbinfo.find { node ->
        dbNamesMatch(node.attributes()['dbname'],db.attributes()['dbname']) }
      entry?.attributes().each {
        k,v -> db.@"$k" = v
        dbIsInXref = true }
    }

    if (dbIsInXref) {

      // scan through all hits and add interpro domains
      analysisXML.protein.each { pNode ->
        pNode.matches?."match"?.each { mNode ->
          def ip = interproXML.interpro.member_list.db_xref.find { node ->
            dbNamesMatch(node.attributes()['db'],dbName) &&
                dbkeyAndAcMatches(node.@dbkey,mNode.signature[0].@ac)
          }
          if (mNode.signature[0].@name == null && ip != null && ip.attributes() != null) {
            // add the name if we don't have one
            mNode.signature[0].@name = ip?.attributes()['name']
          }
          // ascend the xml until we find the parent from interpro
          // this should just be parent().parent()
          def parentNode = ip;
          def upOne = ip?.parent();
          while (parentNode != null && parentNode.name() != 'interpro' ) {
            parentNode = upOne;
            upOne = parentNode.parent();
          }
          if (parentNode != null) {
            def newNode = new Node(mNode.signature[0],'entry')
            newNode.@id = parentNode.@id
            newNode.@name = parentNode.@short_name
            // does this have GO terms?
            def goNodes = parentNode.class_list.classification.findAll { node ->
              node.@class_type == 'GO'}
            goNodes?.each { goNode -> def newGoNode = new Node(newNode,'go-xref')
              // some twists and turns here
              newGoNode.@id = goNode.@id
              newGoNode.@db = 'GO'
              newGoNode.@category = goNode.category.text().toUpperCase().replaceAll(' ','_')
              newGoNode.@name = goNode.description.text()

            }
            //  }
          }
        }
      }
    }
    XmlUtil.serialize(analysisXML)
  }

  // this encapsulates how we compare dbkey in the interpro.xml file
  // to the 'ac' from the match hit name in the program run. This may
  // be program specific and will be overridden in a subclass
  def dbkeyAndAcMatches(String a,String b) {
    return a==b
  }
  // and how we compare db names
  def dbNamesMatch(String a,String b) {
    return a==b
  }
}

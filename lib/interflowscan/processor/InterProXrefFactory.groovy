/**
 * 
 */
package interflowscan.processor
import groovy.util.XmlParser
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
/**
 * @author jcarlson
 *
 */
class InterProXrefFactory {

  static main(String[] args) {
    // if we're called as a program,
    // the first is the interpro.xml
    // the second argument is the program type,
    // the third is the input
    // and the fourth is the ouput
    if (args.size() > 3) {
      def factory = new InterProXrefFactory(args[0])
      def transformer = factory.getTransformer(args[1]).newInstance();
      def xmlOut = new File(args[3]);
      xmlOut.write(transformer.process(factory.interproXML,new File(args[2])));
    }
  }

  def interproXML
  def loader

  InterProXrefFactory(String a) {

    def parser = new XmlParser(false,false,true)
    parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
    parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
    interproXML = parser.parse(a)
    loader = this.getClass().getClassLoader()

  }
  
  Class getTransformer(type) {

    // change this_program_name to thisProgramName
    def bits = type.tokenize('_');
    def modType = bits[0]
    if ( bits.size() > 1) {
      bits[1..-1].each{s->modType += s[0].toUpperCase()+s[1..-1]}
    }

    // try to load type-specific xref'er. If not found, fall back to generic
    def instance
    try {
      instance = loader.loadClass('interflowscan.processor.'+modType+'Xref')
    } catch (ClassNotFoundException e) {
      instance = loader.loadClass('interflowscan.processor.InterProXref')
    }
 
  }

}

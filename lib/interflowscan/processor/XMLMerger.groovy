/**
 * 
 */
package interflowscan.processor

import groovy.util.XmlParser
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import groovy.util.Node
import groovy.util.logging.*
import org.apache.log4j.*

/**
 * @author jcarlson
 *
 */
@Log4j
class XMLMerger {

  /* Merge a set of interpro xml files into 1. The first
   * argument is a list with elements a pair of a program name and
   * an XML file: [ [program1,xml1], [program2,xml2], ...]
   * The second argument is an XML file to write to.
   */

  def static process(a) {

    log.level = Level.INFO
    def xml = []
    a.each{ log.info('Parsing '+it[1]+'...')
      xml << [it[0],  new XmlParser().parse('file://'+it[1])] }
    log.info('Parsed results.')

    // start with a bare document
    def outXml = new XmlParser().parseText("<protein-matches/>")
    // add a release node
    def releaseNode = new Node(outXml,'release');

    // add nodes for dbinfo in each source document
    // but do not double insert
    HashSet<String> processedDb = new HashSet<String>();
    xml.each { parent -> def relNode = parent[1].release.dbinfo.first()
      if ( !processedDb.contains(relNode.@dbname) ) {
        new Node(releaseNode,'dbinfo',relNode?.attributes())
        processedDb.add(relNode.@dbname)
      }
    }

    // scan source documents for every protein id
    def proteinIds = new HashMap<String,ArrayList>()
    xml.each { parent -> parent[1].protein.each { it.xref.@id.each { p ->
          if ( !proteinIds.containsKey(p)) { proteinIds.put(p, new HashSet())}
          proteinIds.get(p).add([parent[0],it]) } }
    }
    log.info('Parsed for proteins.')

    def ctr = 0

    // now go though every protein id and add all nodes for that protein.
    proteinIds.each { proteinId, protNodes ->
      ctr++
      if ( ctr%1000 == 0 ) {
        log.info('Processing protein '+ctr+'.')
      }
      def protNode = new Node(outXml,'protein')
      def newXrefNode = new Node(protNode,'xref',[id:proteinId])
      def newMatchesNodes = new Node(protNode,'matches')
      protNodes.each { proNode ->
        proNode[1].matches?."match"?.each {  matchNode ->
          def progMatchNode = new Node(newMatchesNodes,proNode[0]+'-match')
          matchNode.children().each { progMatchNode.append(it.clone())}
        }
      }
    }

    // the text content of the results. Broken out by program
    def textContent = [:]
    // now process the xml for the "to-be-processed-later" analysis results
    xml.each { parent -> parent[1].results.each { results ->
        //println 'adding info for '+parent[0]+' with size'+results.text().size()+' to '+textContent[parent[0]]?.size()
        if (textContent[parent[0]]==null ) {
          textContent[parent[0]] = []
        }
        textContent[parent[0]] << results.text()
      }
    }
    //println 'now adding content to node...'

    textContent.each { prog, content ->
      def block = new Node(outXml,prog+'-results')
      content.each { new Node(block,prog+'-results-block',it)
      }
    }

    //println 'done'
    log.info('returning xml')

    return XmlUtil.serialize(outXml);

  }

}

package interflowscan.processor

class pfamXref extends InterProXref {
  
  def pfamXref() {}
  
  // remove any version numbers from PF program run
  def dbkeyAndAcMatches(String a, String b) {
    return a==b.tokenize('.')[0]
  }
}

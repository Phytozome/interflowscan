/**
 * XMLCompare: do a comparison of in InterFlowScan xml file with a InterProScan5 xml file 
 */
package interflowscan
import groovy.util.XmlParser
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import groovy.util.Node


/**
 * @author jcarlson
 *
 */
class XMLCompare {

  static main(args) {

    def newOldMatchName = ['phobius-match':'phobius-match','superfamily-match':'superfamilyhmmer3-match',]

    def newXML = new XmlParser().parse(args[0])
    def refXML = new XmlParser().parse(args[1])
    println 'Parsed results.'
  
    def foundHits = new HashMap<String,HashSet<String>>()
    def notFoundHits = new HashMap<String,HashSet<String>>()

    newXML.protein.each { newNode -> newNode.xref.@id.each { p ->
        def refNode = refXML.protein.find { node -> node.xref.find { xrf -> xrf.@id == p } }
        if (refNode == null) {
          println 'cannot find protein '+p
        } else {
          def refMatches = refNode.matches
          def newMatches = newNode.matches
          println 'processing protein '+p
          assert refMatches.size() == 1
          assert newMatches.size() == 1
          def newMatch = newMatches[0]
          def refMatch = refMatches[0]
          newMatch.children().each { x -> 
            def compNode = refMatch.children().find { y -> y.signature[0].@ac.tokenize('.')[0] == x.signature[0].@ac.tokenize('.')[0] }
            if (compNode == null) {
              if (! notFoundHits.containsKey(x.name()) ) {
                notFoundHits.put(x.name(),new HashSet<String>())
              }
              notFoundHits.get(x.name()).add(x.signature[0].@ac)
              //println 'did not find ac for '+x.name()+' hit to '+x.signature[0].@ac
            } else {
              if (! foundHits.containsKey(x.name()) ) {
                foundHits.put(x.name(),new HashSet<String>())
              }
              foundHits.get(x.name()).add(x.signature[0].@ac)
              //println 'found ac for '+x.name()+' hit to '+x.signature[0].@ac
            }
          }
        }
      }
    }
    
    println 'found Hits: '
    foundHits.each{ k,v -> println '\tprogram: '+k+' '+v.size()+' entries: '+v }
    println '\nnot found: '
    notFoundHits.each{ k,v -> println '\tprogram: '+k+' '+v.size()+' entries: '+v }
  }
}

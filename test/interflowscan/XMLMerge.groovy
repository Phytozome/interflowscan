/** Test and run the xml merger code.
 * 
 */
package interflowscan

import groovy.util.logging.*
import org.apache.log4j.*
import interflowscan.processor.XMLMerger

 /* @auscarlson
 *
 */
@Log4j
class XMLMerge {
  
  
  static main(args) {

    /* the arguments are a series of xml files with an optiona
     * programname: prefix. If not supplied, a generic 'progN:' is
     * used
     */
    
    log.level = Level.INFO
    log.info("Running with args "+args)

    def xmlToMerge = [];
    def ctr = 1;
    args.each { arg ->
      def bits = arg.tokenize(':')
      xmlToMerge << ((bits.size() == 2)? bits : ['prog'+ctr, arg])
      ctr++ }
    
    log.info("After transformation " +xmlToMerge)
    
    XMLMerger.process(xmlToMerge,"testMergeOut.xml")

  }
}


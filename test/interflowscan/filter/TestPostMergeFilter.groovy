package interflowscan.filter

import interflowscan.filter.postMergeFilter

/**
 * @author jcarlson
 *
 */
def b= new File("/global/projectb/scratch/jcarlson/interflowscan/wheat/T.aestivum_296_kegg_only.xml_raw");
def output = postMergeFilter.process(b,"-evalue 1.e-10 -cscore 0.95 "+
  "-output /global/projectb/scratch/jcarlson/interflowscan/wheat/T.aestivum_296_kegg_only.xml -map /global/projectb/sandbox/plant/interproscan-data/data/kegg/v75/ko_genes.list")

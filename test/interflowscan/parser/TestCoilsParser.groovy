/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.coilsParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/coils.out")
def output = coilsParser.parse(b)

def file = new File("test/resources/coils.xml")
file.delete()
file << output

def rawHits="egrep -c  ^[1-9] test/resources/coils.out".execute().text.trim().toInteger()
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/coils.xml")
assert xmlHits == rawHits;
println "Got them."

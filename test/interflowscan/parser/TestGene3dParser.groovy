/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.gene3dParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/gene3d.out");
def output = gene3dParser.parse(b)

file = new File("test/resources/gene3d.xml")
file.delete()
file << output

def rawHits = 0;
b.eachLine { str ->
  // count the numeber of :'s on each line. a:b:c:d:e:f...
  // (that number +1)/2 is the number of hits
  def matchGroup = (str =~ /:/);
  rawHits += (matchGroup.size()+1)/2
}

println "Expect $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/gene3d.xml")
assert xmlHits == rawHits;
println "Got them."

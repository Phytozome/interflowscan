/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.keggParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/kegg.out");
def output = keggParser.parse(b)

def file = new File("test/resources/kegg.xml")
file.delete()
file << output

def rawHits = "egrep -vc # test/resources/kegg.out".execute().text.trim().toInteger()
println "Expected $rawHits hits."
def xmlHits = "egrep -vc < test/resources/kegg.xml".execute().text.trim().toInteger()
assert xmlHits == rawHits;
println "Got them."

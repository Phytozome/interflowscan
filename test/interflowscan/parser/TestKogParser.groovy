/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.kogParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/kog.out");
def output = kogParser.parse(b)

def file = new File("test/resources/kog.xml")
file.delete()
file << output

def rawHits = "egrep -c Hsp_score test/resources/kog.out".execute().text.trim().toInteger()
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/kog.xml")

assert xmlHits == rawHits;
println "Got them."

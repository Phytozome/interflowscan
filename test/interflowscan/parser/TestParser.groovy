/**
 * 
 */
package interflowscan.parser

/**
 * @author jcarlson
 *
 */
class TestParser {

  static int countLocation(String file) {
    def xmlHits = 0
    "egrep location $file".execute().text.eachLine { str ->
      if (str.trim() ==~ /<location start=.*/) {
        xmlHits++
      }
    }
    return xmlHits
  }

}

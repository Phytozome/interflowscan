/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.pfamParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/pfam.out");
def output = pfamParser.parse(b)

def file = new File("test/resources/pfam.xml")
file.delete()
file << output

def rawHits = 0
def inBlock = false;
"egrep -v # test/resources/pfam.out".execute().text.eachLine { str ->
  if (str.trim() ==~ /Domain annotation.*/ ) {
    inBlock = true;
  } else if ( str.trim() ==~ /Internal pipeline.*/ ) {
    inBlock = false;
  }
  if (inBlock && (str.trim() ==~ /[1-9].*/)) {
    rawHits++
  }
}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/pfam.xml")

assert xmlHits == rawHits;
println "Got them."

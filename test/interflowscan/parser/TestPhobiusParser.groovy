/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.phobiusParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/phobius.out");
def output = phobiusParser.parse(b)

def file = new File("test/resources/phobius.xml")
file.delete()
file << output

def rawHits = "egrep -c ^FT test/resources/phobius.out".execute().text.trim().toInteger()
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/phobius.xml")

assert xmlHits == rawHits;
println "Got them."

/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.pirsfParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/pirsf.out");
def output = pirsfParser.parse(b)

def file = new File("test/resources/pirsf.xml") 
file.delete()
file << output

def rawHits = 0
"egrep -v Query test/resources/pirsf.out".execute().text.eachLine { str ->
  if (str.trim() ==~ /[1-9].*/ ) {
    rawHits++
  }
}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/pirsf.xml")
assert xmlHits == rawHits;
println "Got them."

/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.prositeParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/prosite.out");
def output = prositeParser.parse(b)

def file = new File("test/resources/prosite.xml")
file.delete()
file << output

def rawHits = "egrep -c . test/resources/prosite.out".execute().text.trim().toInteger()
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/prosite.xml")

assert xmlHits == rawHits;
println "Got them."

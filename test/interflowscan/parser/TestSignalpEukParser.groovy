/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.signalpEukParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/signalp_euk.out");
def output = signalpEukParser.parse(b)

def file = new File("test/resources/signalp_euk.xml")
file.delete()
file << output

def rawHits=0
"egrep ^Name test/resources/signalp_euk.out".execute().text.eachLine { str ->
  if (str.trim() ==~ /.*SP='YES'.*/ ) {
    rawHits++
  }
}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/signalp_euk.xml")

assert xmlHits == rawHits;
println "Got them."

/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.signalpGramNegativeParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/signalp_gramnegative.out");
def output = signalpGramNegativeParser.parse(b)

def file = new File("test/resources/signalp_gramnegative.xml")
file.delete()
file << output

def rawHits=0
"egrep ^Name test/resources/signalp_gramnegative.out".execute().text.eachLine { str ->
  if (str.trim() ==~ /.*SP='YES'.*/ ) {
    rawHits++
  }
}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/signalp_gramnegative.xml")

assert xmlHits == rawHits;
println "Got them."

/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.signalpGramPositiveParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/signalp_grampositive.out");
def output = signalpGramPositiveParser.parse(b)

def file = new File("test/resources/signalp_grampositive.xml")
file.delete()
file << output

def rawHits=0
"egrep ^Name test/resources/signalp_grampositive.out".execute().text.eachLine { str ->
  if (str.trim() ==~ /.*SP='YES'.*/ ) {
    rawHits++
  }
}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/signalp_grampositive.xml")

assert xmlHits == rawHits;
println "Got them."

/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.smartParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/smart.out");
def output = smartParser.parse(b)

def file = new File("test/resources/smart.xml")
file.delete()
file << output


def rawHits=0
def inBlock = false
b.eachLine { str ->
  if (str.trim() ==~ /Model    Domain.*/ ) {
    inBlock = true;
  } else if (str.trim().length() == 0) {
    inBlock = false;
  } else if (inBlock && !(str ==~ /[\s-].*/) ) {
    rawHits++
  }
}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/smart.xml")

assert xmlHits == rawHits;
println "Got them."

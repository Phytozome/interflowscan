/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.superfamilyParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File(args?args[0]:"test/resources/superfamily.out");
def output =  superfamilyParser.parse(b)

def file = new File("test/resources/superfamily.xml")
file.delete()
file << output

def rawHits = 0
def inBlock = false;
"egrep <h1> test/resources/superfamily.out".execute().text.eachLine { str ->
  str.split(/<.tr><tr>/).each { rec ->
    def fields = rec.split(/<.th><th>/)
    if (fields.size() > 2) {
      numbers = fields[1].split(/<br>/)
      if (! (numbers.size() == 1 && (numbers[0] == '-' || numbers[0] == 'Match_region'))) {
        //println numbers+ ' with size '+numbers.size()
        rawHits += numbers.size()
      }
    }
  }

}
println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/superfamily.xml")

assert xmlHits == rawHits;
println "Got them."

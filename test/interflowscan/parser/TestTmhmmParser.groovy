/**
 * 
 */
package interflowscan.parser

import interflowscan.parser.tmhmmParser
import interflowscan.parser.TestParser
/**
 * @author jcarlson
 *
 */
def b= new File("test/resources/tmhmm.out");
def output = tmhmmParser.parse(b)

def file = new File("test/resources/tmhmm.xml")
file.delete()
file << output

def rawHits = "egrep -vc ^# test/resources/tmhmm.out".execute().text.trim().toInteger()

println "Expected $rawHits hits."

def xmlHits = TestParser.countLocation("test/resources/tmhmm.xml")

assert xmlHits == rawHits;
println "Got them."
